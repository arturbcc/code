﻿using Arenah.Duels.Models;
using Neverend.Arena.Model;

namespace Arenah.Duels.ViewData
{
    public class DuelViewData: BaseViewData
    {
        public Duel CurrentDuel { get; set; }
        public UserAccount ChallengerUserAccount { get; set; }
        public UserAccount ChallengedUserAccount { get; set; }

        public UserAccount Vanquished { get; set; }
        public UserAccount Victor { get; set; }
    }
}
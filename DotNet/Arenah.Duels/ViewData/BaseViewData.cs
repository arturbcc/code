﻿using Neverend.Arena.Model;
using Neverend.Arena.Core;
using PassportManager.Manager;
using Neverend.Arena.Manager;
using System;
using Arenah.Duels.Models;
using System.Collections.Generic;

namespace Arenah.Duels.ViewData
{
    public class BaseViewData
    {
        public string Message { get; set; }
        public UserPartner LoggedUser { get; set; }
        public UserAccount Character { get; set; }
        public LinkHelper LinkHelper { get; set; }
        public List<UserAccount> Characters{ get; set; }
        public List<Duel> Duels { get; set; }
        public Character DuelCharacter { get; set; }
        public CharacterData CharacterData { get; set; }

        public BaseViewData()
        {
            UserPartner loggedUser = new AuthenticationManager().GetLoggedUser(Factory.partner.PartnerId);
            if (loggedUser != null)
            {
                loggedUser.PassportUser = new UserManager().GetUserNoCache(loggedUser.PassportId);
                Character = Neverend.Arena.API.UserAccountManager.GetUserAccountByUserPartnerIdAndForumId(loggedUser.UserId, Factory.forumId);
            }
            LoggedUser = loggedUser;
            LinkHelper = new LinkHelper();
        }
    }
}
﻿using System.Collections.Generic;
using Arenah.Duels.Models;

namespace Arenah.Duels.ViewData
{
    public class MessageViewData: BaseViewData
    {
        public List<Message> Messages { get; set; }
        public Message CurrentMessage { get; set; }
        public Character Victor { get; set; }
        public Character Vanquished { get; set; }
    }
}
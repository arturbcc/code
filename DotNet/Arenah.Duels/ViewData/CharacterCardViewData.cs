﻿using Neverend.Arena.Model;
using System.Reflection;
using System;

namespace Arenah.Duels.ViewData
{
    public class CharacterCardViewData: BaseViewData
    {
        public UserAccount Account { get; set; }
        public bool HideBackButton { get; set; }

        public CharacterCardViewData(): base()
        {
            HideBackButton = false;
        }


        public CharacterCardViewData(BaseViewData parent)
        {
            foreach (PropertyInfo prop in parent.GetType().GetProperties())
                GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(parent, null), null);
        }

        public CharacterCardViewData(DuelViewData parent)
        {
            foreach (PropertyInfo prop in parent.GetType().GetProperties())
            {
               Type type = GetType();
               PropertyInfo propertyInfo = type.GetProperty(prop.Name);
               if (propertyInfo != null)
               {
                   propertyInfo.SetValue(this, prop.GetValue(parent, null), null);
               }
            }
                
        }
    }
}
﻿using System;
using System.Drawing;
using Neverend.Arena.Model;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Arenah.Duels.Models
{
    public class ImageCombiner
    {
        public static void CombineTwoImages(Guid characterId)
        {
            string name = String.Format("{0}{1}_blood.png", WebConfig.GetKey("imagesPhysicalPath"), characterId);
            UserAccount character = Neverend.Arena.API.UserAccountManager.GetUserAccountById(characterId);
            string fileName = Path.GetFileNameWithoutExtension(character.AvatarUrl);
            string path = WebConfig.GetKey("avatarPhysicalPath");

            if (!File.Exists(name))
            {
                System.Drawing.Graphics myGraphic = null;
                //Image characterAvatar = Image.FromFile(string.Format("{0}{1}.jpg", path, fileName));
                Image characterAvatar = ImageZoom(Image.FromFile(string.Format("{0}{1}.jpg", path, fileName)), new Size(250, 250));
                Image blood = Image.FromFile(string.Format("{0}{1}", WebConfig.GetKey("imagesPhysicalPath"), "bloodStain.png"));
                //Image blood = ImageZoom(Image.FromFile(string.Format("{0}{1}", WebConfig.GetKey("imagesPhysicalPath"), "blood2.png")), new Size(250, 250));
                //Image finalImage = Image.FromFile(string.Format("{0}{1}.jpg", path, fileName));
                Image finalImage = ImageZoom(Image.FromFile(string.Format("{0}{1}.jpg", path, fileName)), new Size(250,250));
                

                myGraphic = System.Drawing.Graphics.FromImage(finalImage);
                myGraphic.DrawImageUnscaled(characterAvatar, 0, 0);
                myGraphic.DrawImageUnscaled(blood, 45, 45);
                myGraphic.Save();

                finalImage.Save(name, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        public static Image ImageZoom(Image image, Size newSize)
        {
            var bitmap = new Bitmap(image, newSize.Width, newSize.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            }

            return bitmap;
        }
    }
}
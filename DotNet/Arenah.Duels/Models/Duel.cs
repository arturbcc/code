﻿using System;
using Boycott;
using Boycott.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arenah.Duels.Models
{
    [NotSynchronizable]
    [Table(Name = "duels")]
    public class Duel : Boycott.Base<Duel>
    {
        [NotSynchronizableAttribute]
        [IgnoreColumnsAttribute]
        public enum STATUS: short
        {
            FINISHED = 0,
            TURN_START = 1, //Both players must choose skills
            WAITING_FOR_TURN = 2 //One player must choose skills
        }

        [NotSynchronizableAttribute]
        [IgnoreColumnsAttribute]
        public enum WINNER_STATUS : short
        {
            NONE = 0,
            CHALLENGER = 1,
            CHALLENGED = 2
        }

        [Column(Name = "Id", IsPrimaryKey = true)]
        [PrimaryKey]
        public int Id { get; set; }

        [Column(Name = "ChallengerId")]
        public Guid ChallengerId { get; set; }
        [Column(Name = "ChallengedId")]
        [PrimaryKey]
        public Guid ChallengedId { get; set; }
        [Column(Name = "CreationDate")]
        public DateTime CreationDate { get; set; }
        [Column(Name = "Status")]
        public int Status { get; set; }
        [Column(Name = "Turn")]
        public int Turn { get; set; }
        [Column(Name = "ChallengerSheet")]
        public string ChallengerSheet { get; set; }
        [Column(Name = "ChallengedSheet")]
        public string ChallengedSheet { get; set; }
        [Column(Name = "Log")]
        public string Log { get; set; }
        [Column(Name = "Winner")]
        public int Winner { get; set; }


        [NotSynchronizableAttribute, IgnoreColumnsAttribute]
        public CharacterData Challenger { get; set; }
        [NotSynchronizableAttribute, IgnoreColumnsAttribute]
        public CharacterData Challenged { get; set; }

        public void PrepareData()
        {
            this.Challenger = new CharacterData(this.ChallengerSheet);
            this.Challenged = new CharacterData(this.ChallengedSheet);
        }

        public static Duel FindLast(Guid challengerId, Guid challengedId)
        {
            return (from duel in Duel.db
                    where duel.ChallengerId == challengerId && duel.ChallengedId == challengedId && duel.Status != (int) Duel.STATUS.FINISHED
                    orderby duel.CreationDate descending
                    select duel).First<Duel>();
        }

        public static List<Duel> DuelsForUserAccountId(Guid userAccountId)
        {
            List<Duel> duels = (from duel in Duel.db
                              where duel.ChallengerId == userAccountId || duel.ChallengedId == userAccountId
                              select duel).ToList<Duel>();

            foreach (Duel duel in duels)
            {
                duel.PrepareData();
            }

            return duels;
        }

        public void AddLogEntry(string text)
        {
            if (this.Log == null)
                this.Log = string.Empty;

            this.Log += "<p>" + text + "</p>";
        }

        public void AddLogNewTurn(string text)
        {
            if (this.Log == null)
                this.Log = string.Empty;

            this.Log += "<h3 class='underlined'>" + text + "<h3>";
        }

    }
}
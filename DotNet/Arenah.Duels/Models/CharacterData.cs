﻿using System.Collections.Generic;
using Common;
using System.Web.Script.Serialization;
using System;

namespace Arenah.Duels.Models
{
    public class CharacterData
    {
        public string Name { get; set; }
        public string Race { get; set; }
        public int Fr { get; set; }
        public int Con { get; set; }
        public int Dex { get; set; }
        public int Agi { get; set; }
        public int Intelligence { get; set; }
        public int Will { get; set; }
        public int Per { get; set; }
        public int Car { get; set; }
        public int Initiative { get; set; }
        public string CharacterType { get; set; }
        public string CharacterClass { get; set; }
        public int PV { get; set; }
        public int TotalPV { get; set; }
        public List<Skill> Skills{ get; set; }
        
        //Used to save skills chosen to the combat
        public List<Skill> ChosenSkills { get; set; }


        public int Effect { get; set; }
        public int TurnsRemaining { get; set; }
        public int BonusAttackValue { get; set; }
        public int BonusDefenseValue { get; set; }

        public enum EFFECT_STATE : short
        {
            NO_EFFECT = 0,
            DEFENSE_BONUS = 1,
            ATTACK_BONUS = 2,
            CONFUSION = 3,
            ATTACK_AND_DEFENSE_BONUS = 5
        }

        public CharacterData(string sheet)
        {
            if (!string.IsNullOrEmpty(sheet))
            {
                IDictionary<string, object> data = ParseData(sheet);
                this.Name = data["Name"].ToString();
                this.Race = data["Race"].ToString();
                this.Fr = Util.ConvertToInteger(data["Fr"]);
                this.Con = Util.ConvertToInteger(data["Con"]);
                this.Dex = Util.ConvertToInteger(data["Dex"]);
                this.Agi = Util.ConvertToInteger(data["Agi"]);
                this.Intelligence = Util.ConvertToInteger(data["Intelligence"]);
                this.Will = Util.ConvertToInteger(data["Will"]);
                this.Per = Util.ConvertToInteger(data["Per"]);
                this.Car = Util.ConvertToInteger(data["Car"]);
                this.Initiative = Util.ConvertToInteger(data["Initiative"]);
                this.CharacterType = data["CharacterType"].ToString();
                this.CharacterClass = data["CharacterClass"].ToString();
                this.PV = Util.ConvertToInteger(data["Pv"]);
                this.TotalPV = Util.ConvertToInteger(data["TotalPv"]);

                if (data.Keys.Contains("Effect"))
                    this.Effect = Util.ConvertToInteger(data["Effect"]);
                else
                    this.Effect = 0;

                if (data.Keys.Contains("TurnsRemaining"))
                    this.TurnsRemaining = Util.ConvertToInteger(data["TurnsRemaining"]);
                else
                    this.TurnsRemaining = 0;

                if (data.Keys.Contains("BonusAttackValue"))
                    this.BonusAttackValue = Util.ConvertToInteger(data["BonusAttackValue"]);
                else
                    this.BonusAttackValue = 0;

                if (data.Keys.Contains("BonusDefenseValue"))
                    this.BonusDefenseValue = Util.ConvertToInteger(data["BonusDefenseValue"]);
                else
                    this.BonusDefenseValue = 0;

                this.Skills = this.AddSkills(data, "Skills");

                if (data.ContainsKey("ChosenSkills"))
                {
                    this.ChosenSkills = this.AddSkills(data, "ChosenSkills");
                }
            }
        }

        public string ToJson()
        {
            return new JavaScriptSerializer().Serialize(this);
        }

        public IDictionary<string, object> ParseData(string sheet)
        {
            var jsonObject = new JavaScriptSerializer().DeserializeObject(sheet) as Dictionary<string, object>;
            Dictionary<string, object> caseInsensitiveDictionary = new Dictionary<string, object>(jsonObject, StringComparer.OrdinalIgnoreCase);
            return caseInsensitiveDictionary;
        }

        public bool ChoseSkills()
        {
            return this.ChosenSkills != null && this.ChosenSkills.Count > 0;
        }

        private List<Skill> AddSkills(IDictionary<string, object> data, string key)
        {
            object[] skills = (object[])data[key];
            List<Skill> list = new List<Skill>();
            if (skills != null)
            {
                Skill skill = null;
                foreach (object jsonSkill in skills)
                {
                    skill = new Skill((IDictionary<string, object>)jsonSkill);
                    skill.BaseValue = Util.ConvertToInteger(data[JsonUtil.UpperCaseFirstCharacter(skill.Base)]);
                    list.Add(skill);
                }
            }

            return list;
        }

        public bool IsAlive()
        {
            return this.PV > 0;
        }

        public void DecreaseTurnCounter()
        {
            this.TurnsRemaining--;
            if (this.TurnsRemaining == 0)
            {
                this.Effect = (int)CharacterData.EFFECT_STATE.NO_EFFECT;
                this.BonusAttackValue = 0;
                this.BonusDefenseValue = 0;
            }
        }

        public int GetBaseValue(string skillBase) 
        {
            int value = 0;
            switch (skillBase.ToLower())
            {
                case "fr":
                    value = this.Fr;
                    break;
                case "con":
                    value = this.Con;
                    break;
                case "dex":
                    value = this.Dex;
                    break;
                case "agi":
                    value = this.Agi;
                    break;
                case "intelligence":
                    value = this.Intelligence;
                    break;
                case "will":
                    value = this.Will;
                    break;
                case "per":
                    value = this.Per;
                    break;
                case "car":
                    value = this.Car;
                    break;
            }
            return value;
        }

        public static CharacterData FromUserAccountId(Guid userAccountId)
        {
            Character currentCharacter = Character.FindById(userAccountId);

            //If user does not exist, create it
            if (currentCharacter == null)
            {
                currentCharacter = new Character();
                currentCharacter.UserAccountId = userAccountId;
                currentCharacter.Sheet = DefaultSheet();
                currentCharacter.Save();
            }

            return new CharacterData(currentCharacter.Sheet);
        }

        public static string DefaultSheet()
        {
            return "{'Name':'', 'Race':'', 'Fr':0, 'Con':0, 'Dex':0, 'Agi':0, 'Intelligence':0, 'Will':0, 'Per':0, 'Car':0, 'Initiative':0, 'CharacterType':'PC', 'CharacterClass':'', 'PV':0, 'TotalPV':0, 'Skills':[], 'ChosenSkills':[]}";
        }
        
    }
}
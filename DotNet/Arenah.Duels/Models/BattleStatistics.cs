﻿namespace Arenah.Duels.Models
{
    public class BattleStatistics
    {
        public int Victories { get; set; }
        public int Defeats { get; set; }
    }
}
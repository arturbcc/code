﻿using System.Web.Mvc;
using System.Web;
using System.Diagnostics;
using PassportLibrary;
using PassportManager.Manager;

namespace Arenah.Duels.Models
{
    public class RequiresAuthentication : ActionFilterAttribute
    {
        private UserCookieManager userCookieManager;
        protected UserCookieManager UserCookieManager
        {
            [DebuggerStepThrough]
            get
            {
                if (userCookieManager == null)
                    userCookieManager = new UserCookieManager();

                return userCookieManager;
            }
        }

        protected User user;
        protected User WebUser
        {
            [DebuggerStepThrough]
            get
            {
                if (user == null)
                {
                    UserManager userManager = new UserManager();
                    user = userManager.GetUser(UserCookieManager.UserId);
                }

                return user;
            }
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (WebUser == null || WebUser.UserId <= 0)
            {
                filterContext.Result = new RedirectResult(string.Format("{0}{1}login", WebConfig.GetKey("ArenahDuelsSite"), WebConfig.GetKey("VirtualDirectory")));
            }   
        }
    }
}

﻿using System.Configuration;
using Common;

namespace Arenah.Duels.Models
{
    public static class WebConfig
    {
        public static string GetKey(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static int GetIntKey(string key)
        {
            return Util.ConvertToInteger(ConfigurationManager.AppSettings[key]);
        }

        public static bool GetBoolKey(string key)
        {
            return Util.ConvertToBoolean(ConfigurationManager.AppSettings[key]);
        }

    }
}
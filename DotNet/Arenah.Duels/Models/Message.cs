﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Boycott;
using Boycott.Attributes;


namespace Arenah.Duels.Models
{
    [NotSynchronizable]
    [Table(Name = "messages")]
    public class Message : Boycott.Base<Message>
    {
        [NotSynchronizableAttribute]
        [IgnoreColumnsAttribute]
        public enum STATUS : short
        {
            NEW_DUEL = 0,
            DUEL_FINISHED = 1,
            DUEL_ON_GOING = 2
        }

        [Column(Name = "Id", IsPrimaryKey = true)]
        [PrimaryKey]
        public int Id { get; set; }
        [Column(Name = "Title")]
        public string Title { get; set; }
        [Column(Name = "ChallengerId")]
        public Guid ChallengerId { get; set; }
        [Column(Name = "ChallengedId")]
        public Guid ChallengedId { get; set; }
        [Column(Name = "DuelId")]
        public int DuelId { get; set; }
        [Column(Name = "Status")]
        public int Status { get; set; }
        [Column(Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

        [NotSynchronizable, IgnoreColumnsAttribute]
        public bool? Read { get; set; }
        [NotSynchronizable, IgnoreColumnsAttribute]
        public Duel Duel { get; set; }

        public static Message CreateMessage(Guid challengerId, Guid challengedId)
        {
            Message message = new Message();
            message.CreationDate = DateTime.Now;
            message.ChallengerId = challengerId;
            message.ChallengedId = challengedId;
            return message;
        }

        public int CharacterStatus(Guid userAccountId)
        {
            int result = 0;
            if (this.Duel != null)
            {
                if (userAccountId == this.Duel.ChallengerId)
                    result = this.Duel.Challenger.IsAlive() ? 1 : 0;
                else
                    result = this.Duel.Challenged.IsAlive() ? 1 : 0;
            }
            return result;
        }
    }
}
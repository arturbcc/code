﻿
namespace Arenah.Duels.Models
{
    public class JsonUtil
    {
        public static string UpperCaseFirstCharacter(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                return string.Format(
                    "{0}{1}",
                    text.Substring(0, 1).ToUpper(),
                    text.Substring(1));
            }

            return text;
        }
    }
}
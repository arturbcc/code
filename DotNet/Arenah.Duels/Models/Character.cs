﻿using System;
using Boycott;
using Boycott.Attributes;
using System.Linq;
using System.Collections.Generic;

namespace Arenah.Duels.Models
{
    [NotSynchronizable]
    [Table(Name = "characters")]
    public class Character : Boycott.Base<Character>
    {
        [Column(Name = "UserAccountId", IsPrimaryKey = true)]
        [DbType(DbType = Boycott.Migrate.DbType.Char)]
        [ColumnLimit(Limit = 36)]
        [NotNullable]
        [PrimaryKey]
        public Guid UserAccountId { get; set; }
        [Column(Name = "Sheet")]
        public string Sheet { get; set; }

        public static Character FindOrCreate(Guid UserAccountId)
        {
            Character character = (from c in Character.db
                                   where c.UserAccountId == UserAccountId
                                   select c).Single<Character>();
            if (character == null)
            {
                character = new Character();
                character.UserAccountId = UserAccountId;
                character.Save();
            }

            return character;
        }

        public static Character FindById(Guid Id)
        {
            List<Character> characters = Character.FindAll(new { UserAccountId = Id });
            return characters.Count > 0 ? characters.First<Character>() : null;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using System.Web.Script.Serialization;

namespace Arenah.Duels.Models
{
    public class Skill
    {
        public string Name { get; set; }
        public string Damage { get; set; }
        public string Base { get; set; }
        public int Initiative { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int BaseValue { get; set; }

        public int Effect { get; set; }
        public int Bonus { get; set; }


        public enum EFFECT_STATE : short
        {
            NO_EFFECT = 0,
            DEFENSE_BONUS = 1,
            ATTACK_BONUS = 2,
            CONFUSION = 3,
            HEALING = 4,
            ATTACK_AND_DEFENSE_BONUS = 5
        }


        public Skill(object skill)
        {
            IDictionary<string, object> data = ParseData(skill);
            this.Name = data["Name"].ToString();
            this.Damage = data["Damage"].ToString();
            this.Base = data["Base"].ToString();
            this.Initiative = Util.ConvertToInteger(data["Initiative"]);
            this.Attack = Util.ConvertToInteger(data["Attack"]);
            this.Defense = Util.ConvertToInteger(data["Defense"]);

            if (data.Keys.Contains("Effect"))
                this.Effect = Util.ConvertToInteger(data["Effect"]);
            else
                this.Effect = 0;

            if (data.Keys.Contains("Bonus"))
                this.Bonus = Util.ConvertToInteger(data["Bonus"]);
            else
                this.Bonus = 0;
        }

        public IDictionary<string, object> ParseData(object skill)
        {
            IDictionary<string, object> data = (IDictionary<string, object>)skill;
            IDictionary<string, object> newData = new Dictionary<string, object>();

            foreach (KeyValuePair<string, object> entry in data)
            {
                newData.Add(JsonUtil.UpperCaseFirstCharacter(entry.Key), entry.Value);
            }

            return newData;
        }
    }
}
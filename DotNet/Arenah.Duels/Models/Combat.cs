﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;

namespace Arenah.Duels.Models
{
    public class Combat
    {
        public Duel Duel { get; set; }

        public Combat(Duel duel) 
        {
            this.Duel = duel;
        }

        public void Start(CharacterData challenger, CharacterData challenged)
        {
            for (int turn = 0; turn < 3; turn++)
            {
                if (challenger.IsAlive() && challenged.IsAlive())
                {
                    Duel.Turn++;
                    this.Duel.AddLogNewTurn(string.Format("Turno {0}", Duel.Turn));
                    List<CharacterData> characters = CalculateInitiative(turn, challenger, challenged);
                    Attack(turn, characters[0], characters[1]);
                    Attack(turn, characters[1], characters[0]);
                    
                    if (characters[0].Effect > 0)
                        characters[0].DecreaseTurnCounter();

                    if (characters[1].Effect > 0)
                        characters[1].DecreaseTurnCounter();
                }
            }
        }

        public List<CharacterData> CalculateInitiative(int turn, CharacterData challenger, CharacterData challenged)
        {
            Skill skillChallenger = challenger.ChosenSkills[turn];
            Skill skillChallenged = challenged.ChosenSkills[turn];

            int dice = 0;

            dice = DiceRoller.Throw(DiceRoller.DiceType.D10);
            challenger.Initiative = dice + skillChallenger.Initiative + challenger.Agi;
            this.Duel.AddLogEntry(string.Format("Iniciativa de {0} = {1} + {2} (Agi) {3} (iniciativa da arma) = {4}", challenger.Name, dice, challenger.Agi, (skillChallenger.Initiative < 0 ? "" : "+ ") + skillChallenger.Initiative, challenger.Initiative));

            dice = DiceRoller.Throw(DiceRoller.DiceType.D10);
            challenged.Initiative = dice + skillChallenged.Initiative + challenged.Agi;
            this.Duel.AddLogEntry(string.Format("Iniciativa de {0} = {1} + {2} (Agi) {3} (iniciativa da arma) = {4}", challenged.Name, dice, challenged.Agi, (skillChallenged.Initiative < 0 ? "" : "+ ") + skillChallenged.Initiative, challenged.Initiative));

            List<CharacterData> characters = new List<CharacterData>();
            if (challenger.Initiative > challenged.Initiative)
            {
                characters.Add(challenger);
                characters.Add(challenged);
            }
            else
            {
                characters.Add(challenged);
                characters.Add(challenger);
            }

            return characters;
        }

        public void Attack(int turn, CharacterData attacker, CharacterData defender)
        {
            if (attacker.IsAlive() && defender.IsAlive())
            {
                Skill attackerSkill = attacker.ChosenSkills[turn];
                Skill defenderSkill = defender.ChosenSkills[3 + turn];

                if (attackerSkill.Effect == 0)
                {
                    int attack = attackerSkill.Attack + attacker.GetBaseValue(attackerSkill.Base);

                    if (attacker.Effect == (int)CharacterData.EFFECT_STATE.ATTACK_AND_DEFENSE_BONUS || attacker.Effect == (int)CharacterData.EFFECT_STATE.ATTACK_BONUS)
                    {
                        attack += attacker.BonusAttackValue;
                        Duel.AddLogEntry(string.Format("{0} tem um bônus de ataque de {1}%", attacker.Name, attacker.BonusAttackValue));
                    }
                    else if (attacker.Effect == (int)CharacterData.EFFECT_STATE.CONFUSION)
                    {
                        Duel.AddLogEntry(string.Format("{0} ficou sem ação e não conseguiu atacar", attacker.Name));
                        return;
                    }

                    int defense = defenderSkill.Defense + defender.GetBaseValue(defenderSkill.Base);

                    if (defender.Effect == (int)CharacterData.EFFECT_STATE.ATTACK_AND_DEFENSE_BONUS || defender.Effect == (int)CharacterData.EFFECT_STATE.DEFENSE_BONUS)
                    {
                        defense = defense + defender.BonusDefenseValue;
                        string turnsToGo = "";
                        if (defender.TurnsRemaining == 1)
                            turnsToGo = "Resta apenas um turno";
                        else
                            turnsToGo = "Restam " + defender.TurnsRemaining + " turnos";
                        Duel.AddLogEntry(string.Format("-- {0} tem um bônus de defesa de {1}%. {2} --", defender.Name, defender.BonusDefenseValue, turnsToGo));
                    }

                    int chance = attack - defense + 50;
                    
                    int dice = DiceRoller.Throw(DiceRoller.DiceType.D100);

                    string status = "";
                    string color = "#000";
                    int damageModifier = 1;
                    if (chance > 0 && dice <= Util.ConvertToInteger(Math.Floor((decimal)chance / 4)))
                    {
                        status = "Acerto crítico";
                        color = "#85229E";
                        damageModifier = 2;
                    }
                    else if (dice >= 95)
                    {
                        status = "Erro crítico";
                        color = "#F00";
                    }
                    else if (dice > chance)
                    {
                        status = "Erro";
                        color = "#C47777";
                    }
                    else
                    {
                        status = "Acerto";
                        color = "#0F0";
                    }

                    Duel.AddLogEntry(string.Format("Ataque de {0}: {1} (chance de acerto = {2}%)", attacker.Name, attackerSkill.Name, attack));
                    Duel.AddLogEntry(string.Format("Defesa de {0}: {1} (chance de defesa = {2}%)", defender.Name, defenderSkill.Name, defense));
                    Duel.AddLogEntry(string.Format("<font color='{0}'>[{1}]</font> {2} x {3}. 1d100 = {4} de {5}", color, status, attacker.Name, defender.Name, dice, chance));

                    if (status == "Acerto crítico" || status == "Acerto")
                        CauseDamage(damageModifier, attacker.Fr, attackerSkill, defender);
                    else if (status == "Erro crítico")
                        CauseDamage(damageModifier, attacker.Fr, attackerSkill, attacker);

                }
                else
                {
                    UseEffect(attacker, defender, attackerSkill);
                }
            }
        }

        public void CauseDamage(int damageModifier, int fr, Skill skill, CharacterData victim)
        {
            Dictionary<string, int> data = CombatUtil.ParseDamage(skill.Damage);
            int initialDamageValue = data["Value"];
            data["Value"] *= damageModifier;
            int bonus = CombatUtil.StrenghtBonus(fr);
            data["Value"] += bonus;

            string strengthBonus = "";
            if (bonus < 0) 
                strengthBonus = " (" + bonus + " de força)";
            else if (bonus > 0) 
                strengthBonus = " (+ " + bonus + " de força)";

            string status = "";
            if (victim.PV - data["Value"] <= 0)
            {
                status = "<font color=red>[RIP]</font> ";
                Duel.Status = (int)Duel.STATUS.FINISHED;
            }

            Duel.AddLogEntry(string.Format("Cálculo do dano: {1}d{2} {3} = {5}(x{0}) {4} => {6}", damageModifier, data["DiceCount"], data["DiceFaces"], data["Modifier"] != 0 ? " + " + data["Modifier"] : "", strengthBonus, initialDamageValue, data["Value"]));
            Duel.AddLogEntry(string.Format("{0}Pontos de vida de {1}: {2} - {3} = {4}", status, victim.Name, victim.PV, data["Value"], victim.PV - data["Value"]));
            victim.PV -= data["Value"];

        }

        public void UseEffect(CharacterData attacker, CharacterData defender, Skill attackerSkill)
        {
            if (attackerSkill.Effect != (int) Skill.EFFECT_STATE.NO_EFFECT)
            {
                Dictionary<string, int> data = CombatUtil.ParseDamage(attackerSkill.Damage);

                int diceCount = data["DiceCount"];
                int diceFaces = data["DiceFaces"];
                int dice = data["Value"];
                
                if (attackerSkill.Effect == (int)Skill.EFFECT_STATE.HEALING)
                {
                    int totalHealed = dice;
                    
                    if (attacker.PV + dice > attacker.TotalPV)
                        totalHealed = Math.Abs(attacker.TotalPV - attacker.PV);

                    attacker.PV += totalHealed;
                    Duel.AddLogEntry(string.Format("{0} recuperou {1} ponto(s) de vida. Total: {2}.", attacker.Name, totalHealed, attacker.PV));
                }
                else if (attackerSkill.Effect == (int)Skill.EFFECT_STATE.CONFUSION)
                {
                    int will = defender.Will * 4;
                    int value = DiceRoller.Throw(DiceRoller.DiceType.D100);
                    if (value > will)
                    {
                        WarnAboutEffectOverlap(defender);
                        defender.Effect = (int)CharacterData.EFFECT_STATE.CONFUSION;
                        defender.TurnsRemaining = dice;
                        Duel.AddLogEntry(string.Format("{0} usou {1}, {2} ficará sem ação por {3} {4}.", attacker.Name, attackerSkill.Name, defender.Name, dice, dice == 1 ? "turno" : "turnos"));
                    }
                    else
                    {
                        Duel.AddLogEntry(string.Format("{0} tentou usar {1}, mas {2} resistiu (teste de will, tirou {3} de {4}).", attacker.Name, attackerSkill.Name, defender.Name, value, will));
                    }
                }
                else if (attackerSkill.Effect == (int)Skill.EFFECT_STATE.ATTACK_BONUS)
                {
                    WarnAboutEffectOverlap(attacker);
                    attacker.Effect = (int)CharacterData.EFFECT_STATE.ATTACK_BONUS;
                    attacker.TurnsRemaining = dice;
                    attacker.BonusAttackValue = attackerSkill.Attack;

                    Duel.AddLogEntry(string.Format("{0} usou {1} e agora tem um bônus de ataque de {2}% por {3} turnos (ou até um novo efeito ser usado).", attacker.Name, attackerSkill.Name, attacker.BonusAttackValue, dice));
                }
                else if (attackerSkill.Effect == (int)Skill.EFFECT_STATE.DEFENSE_BONUS)
                {
                    WarnAboutEffectOverlap(attacker);
                    attacker.Effect = (int)CharacterData.EFFECT_STATE.DEFENSE_BONUS;
                    attacker.TurnsRemaining = dice;
                    attacker.BonusDefenseValue = attackerSkill.Defense;

                    Duel.AddLogEntry(string.Format("{0} usou {1} e agora tem um bônus de defesa de {2}% por {3} turnos (ou até um novo efeito ser usado).", attacker.Name, attackerSkill.Name, attacker.BonusDefenseValue, dice));
                }
                else if (attackerSkill.Effect == (int)Skill.EFFECT_STATE.ATTACK_AND_DEFENSE_BONUS)
                {
                    WarnAboutEffectOverlap(attacker);
                    attacker.Effect = (int)CharacterData.EFFECT_STATE.ATTACK_AND_DEFENSE_BONUS;
                    attacker.TurnsRemaining = dice;
                    attacker.BonusAttackValue = attackerSkill.Attack;
                    attacker.BonusDefenseValue = attackerSkill.Defense;
                    Duel.AddLogEntry(string.Format("{0} usou {1} e agora tem um bônus de ataque de {2}% e de defesa de {3}% por {4} turnos (ou até um novo efeito ser usado).", attacker.Name, attackerSkill.Name, attackerSkill.Attack, attackerSkill.Defense, dice));
                }                
            }
        }

        private void WarnAboutEffectOverlap(CharacterData character)
        {
            if (character.Effect == (int)CharacterData.EFFECT_STATE.ATTACK_AND_DEFENSE_BONUS)
            {
                Duel.AddLogEntry(string.Format("{0} perdeu os bônus de ataque e defesa", character.Name));
            }
            else if (character.Effect == (int)CharacterData.EFFECT_STATE.ATTACK_BONUS)
            {
                Duel.AddLogEntry(string.Format("{0} perdeu os bônus de ataque", character.Name));
            }
            else if (character.Effect == (int)CharacterData.EFFECT_STATE.CONFUSION)
            {
                Duel.AddLogEntry(string.Format("{0} saiu do estado de confusão", character.Name));
            }
            else if (character.Effect == (int)CharacterData.EFFECT_STATE.DEFENSE_BONUS)
            {
                Duel.AddLogEntry(string.Format("{0} perdeu os bônus de defesa", character.Name));
            }

        }

    }
}
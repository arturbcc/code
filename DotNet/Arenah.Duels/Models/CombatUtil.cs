﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using Common;

namespace Arenah.Duels.Models
{
    public class CombatUtil
    {
        public static int StrenghtBonus(int strenght)
        {
            int bonus = 0;
            switch (strenght)
            {
                case 1:
                case 2: bonus = -3;
                    break;
                case 3:
                case 4: bonus = -2;
                    break;
                case 5:
                case 6: bonus = -1;
                    break;
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14: bonus = 0;
                    break;
                case 15:
                case 16: bonus = 1;
                    break;
                case 17:
                case 18: bonus = 2;
                    break;
                case 19:
                case 20: bonus = 3;
                    break;
                case 21:
                case 22: bonus = 4;
                    break;
                case 23:
                case 24: bonus = 5;
                    break;
                case 25:
                case 26: bonus = 6;
                    break;
                case 27:
                case 28: bonus = 7;
                    break;
                case 29:
                case 30: bonus = 8;
                    break;
                case 31:
                case 32: bonus = 9;
                    break;
                case 33:
                case 34: bonus = 10;
                    break;
                case 35:
                case 36: bonus = 11;
                    break;
                case 37:
                case 38: bonus = 12;
                    break;
                case 39:
                case 40: bonus = 13;
                    break;
                case 41:
                case 42: bonus = 14;
                    break;
                case 43:
                case 44: bonus = 15;
                    break;
                case 45:
                case 46: bonus = 16;
                    break;
                case 47:
                case 48: bonus = 17;
                    break;
                case 49:
                case 50: bonus = 18;
                    break;
            }

            return bonus;
        }

        public static Dictionary<string, int> ParseDamage(string damage)
        {
            Dictionary<string, int> data = new Dictionary<string, int>();
            string[] parts = damage.Split('d');
            int diceCount = Util.ConvertToInteger(parts[0]);

            string newDamage = damage.Substring(parts[0].ToString().Length + 1);
            
            parts = newDamage.Split('+');
            int diceType = Util.ConvertToInteger(parts[0]);

            int modifier = 0;
            if (parts.Length > 1)
            {
                modifier = Util.ConvertToInteger(parts[1].ToString());
            }

            int result = 0;
            for (var i = 0; i < diceCount; i++)
            {
                result += DiceRoller.Throw(diceType);
            }
            result += modifier;

            data["DiceCount"] = diceCount;
            data["DiceFaces"] = diceType;
            data["Modifier"] = modifier;
            data["Value"] = result;

            return data;
        }
    }
}
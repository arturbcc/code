﻿using System;
using Boycott;
using Boycott.Attributes;

namespace Arenah.Duels.Models
{
    [NotSynchronizable]
    [Table(Name = "shares")]
    public class Share : Boycott.Base<Share>
    {
        [Column(Name = "Id", IsPrimaryKey = true)]
        [NotNullable]
        [PrimaryKey]
        public int Id { get; set; }

        [Column(Name = "UserAccountId")]
        [DbType(DbType = Boycott.Migrate.DbType.Char)]
        [ColumnLimit(Limit = 36)]
        public Guid UserAccountId { get; set; }

        [Column(Name = "MessageId")]
        public int MessageId { get; set; }

        [Column(Name = "DuelId")]
        public int DuelId { get; set; }

        [Column(Name = "CreationDate")]
        public DateTime CreationDate { get; set; }

    }
}
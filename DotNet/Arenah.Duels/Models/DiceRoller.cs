﻿using System;

namespace Arenah.Duels.Models
{
    public class DiceRoller
    {
        public static Random randomClass = new Random((int)DateTime.Now.Ticks);

        public enum DiceType
        {
            D4 = 4,
            D6 = 6,
            D8 = 8,
            D10 = 10,
            D12 = 12,
            D20 = 20,
            D100 = 100
        }

        public static int Throw(DiceType diceType)
        {
            return randomClass.Next(1, (int)diceType + 1);
        }

        public static int Throw(int diceValue)
        {
            if (diceValue > 0)
            {
                return randomClass.Next(1, diceValue + 1);
            }
            else return 0;
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.MessageViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#challenges">Topo</a></li>
            <li><a href="<%= path %>">Início</a></li>
		</ul>
	</nav>

<% 
    //Character logged in
    UserAccount character = ViewData.Model.Character;
    
    //Account of the card
    UserAccount vanquished = Neverend.Arena.API.UserAccountManager.GetUserAccountById(ViewData.Model.Vanquished.UserAccountId);
    UserAccount victor = Neverend.Arena.API.UserAccountManager.GetUserAccountById(ViewData.Model.Victor.UserAccountId);

    Message message = ViewData.Model.CurrentMessage;
    UserAccount challenger = Neverend.Arena.API.UserAccountManager.GetUserAccountById(message.ChallengerId);

%>
    <div class="wrapper wrapper-style3">
        <article id="challenges">
			<div class="container">
                <div class="row">
                    <div class="4u characterCard messageBox">
                        <article class="box box-style2 main" style="float: left">		                    
                            <img src="<%= path %>Content/images/<%= vanquished.UserAccountId %>_blood.png" alt="<%= vanquished.NickName %>" />
                            <h3>
                                <%= challenger.NickName %> <%= message.Title %>
                            </h3>

                            <a href="<%= ViewData.Model.LinkHelper.GetDuelLink(message.DuelId) %>" class="character"><%= victor.NickName%> derrotou <%= vanquished.NickName %> na Arenah de duelos</a>
                         </article>
                    </div>
                    <div class="4u characterCard messageBox">
                        <article class="box box-style2 main" style="float: left">		   
                            <a href="http://www.youtube.com/watch?v=Onga_pGB314">
                                <img src="<%= path %>Content/Images/tour.png" alt="Arenah Tsuki" />
                            </a>
                            <h3>
                                Arenah Tsuki
                            </h3>

                            <a href="http://rpg.arenah.com.br" class="character">Mais que um fórum, um sistema online de RPG</a>
                         </article>
                    </div>
                </div>
            </div>
        </article>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <% 
        string path = Url.VirtualContent();
        UserAccount vanquished = Neverend.Arena.API.UserAccountManager.GetUserAccountById(ViewData.Model.Vanquished.UserAccountId);
        
        //Character logged in
        UserAccount character = ViewData.Model.Character;
    
        //Account of the card
        UserAccount victor = Neverend.Arena.API.UserAccountManager.GetUserAccountById(ViewData.Model.Victor.UserAccountId);

        Message message = ViewData.Model.CurrentMessage;
        UserAccount challenger = Neverend.Arena.API.UserAccountManager.GetUserAccountById(message.ChallengerId);


        
    %>
    <title>Arenah Duelos</title>
    <!-- for Google -->
    <meta name="description" content="Um sistema de RPG online com recursos para mestres e jogadores. Conheça nossa arena de duelos." />
    <meta name="keywords" content="arenah, duelos, rpg, fórum, play-by-forum" />

    <meta name="author" content="Arenah Tsuki" />
    <meta name="copyright" content="Arenah Tsuki" />
    <meta name="application-name" content="Duelos Arenah" />

    <!-- for Facebook -->          
    <meta property="og:title" content="Arenah Tsuki" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<%= WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") %><%= path %>Content/images/<%= vanquished.UserAccountId %>_blood.png?123" />
    <meta property="og:url" content="http://duelos.arenah.com.br/aviso.mvc/91" />
    <meta property="og:description" content="<%= victor.NickName%> derrotou <%= vanquished.NickName %> na Arenah de duelos. <%= challenger.NickName %> <%= message.Title %>." />

    <!-- for Twitter -->          
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Arenah Tsuki" />
    <meta name="twitter:description" content="<%= victor.NickName%> derrotou <%= vanquished.NickName %> na Arenah de duelos. <%= challenger.NickName %> <%= message.Title %>." />
    <meta name="twitter:image" content="<%= WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") %><%= path %>Content/images/<%= vanquished.UserAccountId %>_blood.png?123" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.MessageViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#challenges">Topo</a></li>
            <li><a href="<%= path %>">Início</a></li>
		</ul>
	</nav>

    <div class="wrapper wrapper-style3">
        <article id="challenges">
			<header>
                <br />
				<h2>Duelos</h2>
                <h3>Clique nas mensagens para ir aos duelos correspondentes</h3>
			</header>
			<div class="container">
                <% int count = 0; %>
                <% foreach (Message message in ViewData.Model.Messages)
                { %>
                    <% if (count == 0) { %>
                        <div class="row">
                    <% } %>
                    <div class="4u characterCard messageBox">
                        <article class="box box-style2 main <%= message.Read.Value ? "read" : "" %>" data-id="<%= message.Id %>">
                            <% UserAccount character = Neverend.Arena.API.UserAccountManager.GetUserAccountById(message.ChallengerId); %>
                            <h3>
                                <% if (message.Status == (int) Message.STATUS.NEW_DUEL) { %>
                                    Novo duelo
                                <% }
                                   else if (message.Status == (int)Message.STATUS.DUEL_FINISHED)
                                   { %>
                                    Duelo encerrado
                                <% } 
                                   else
                                   { %>
                                    Duelo em andamento
                                <% } %>
                            </h3>
                            <p>
                            <% if (message.Status != (int) Message.STATUS.DUEL_FINISHED)
                               { %>
                                <a href="<%= ViewData.Model.LinkHelper.GetChooseActionLink(message.DuelId) %>" class="character"><%= character.NickName %> <%= message.Title %></a>
                            <% } else { %>
                                <a href="<%= ViewData.Model.LinkHelper.GetDuelLink(message.DuelId) %>" class="character"><%= character.NickName %> <%= message.Title %></a>
                            <% } %>
                                <br />
                            </p>

                            <% if (message.Status == (int)Message.STATUS.DUEL_FINISHED)
                               { %>
                                <strong>Você <span class="characterStatus"><%= message.CharacterStatus(ViewData.Model.Character.UserAccountId) == 1 ? "venceu" : "perdeu" %></span></strong>
                            <% } %>
                            em <%= message.CreationDate.ToString("dd/MM/yyyy")%>            
                            <p><a href="<%= ViewData.Model.LinkHelper.GetDeleteMessageLink(message.Id) %>" onclick="javascript:if (!confirm('Tem certeza que deseja excluir essa mensagem?')) return false;" class="deleteMessage">[Clique aqui para excluir a mensagem]</a></p>
                            <% if (!message.Read.Value && message.CharacterStatus(ViewData.Model.Character.UserAccountId) == 1)
                               { %>
                                <br />
                                <div class="shareButton" data-message-id="<%= message.Id %>"
                                    data-message="<%= ViewData.Model.Character.NickName%> derrotou <%= character.NickName %>. <%= character.NickName %> <%= message.Title %>."
                                    style="margin: 0 auto">&nbsp;</div>
                            <% } %>
	                    </article>
                    </div>
                    <% if (count == 2)
                       { %>
                        </div>
                        <% count = 0; %>
                    <% }
                       else
                       { %>
                        <% count++; %>
                    <% } %>
               <% } %>
               </div>
			</div>
		</article>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Mensagens</title>
    <% string path = Url.VirtualContent(); %>
    <link href="<%= path %>Content/Styles/jquery.share.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- https://github.com/iatek/jquery-share -->
    <script src="<%= path %>Content/Scripts/jquery.share.js" type="text/javascript"></script>
    <script language="javascript">
        $(document).ready(function () {
            var mvc = $("#mvc").val();
            $.each($('.shareButton'), function () {
                var self = this;
                $(this).share({
                    networks: ['facebook', 'twitter'],
                    urlToShare: 'http://duelos.arenah.com.br/aviso' + mvc + '/' + $(self).attr("data-message-id"),
                    text: $(self).attr("data-message")
                });
            });
        });        
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.BaseViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#top">Topo</a></li>
			<li><a href="#challenges">Desafie</a></li>
            <li><a href="<%= path %>avisos" class="menu-messages">Mensagens</a></li>
			<li><a href="#alerts">Contato</a></li>
            <li><a href="<%= WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") %>dologout">Sair</a></li>
		</ul>
	</nav>

    <% UserAccount character = ViewData.Model.Character; %>
    
    <!-- Home -->
    <div class="wrapper wrapper-style1 wrapper-first">
        <article class="container" id="top" data-id="<%= character.UserAccountId %>">
			<div class="row">
				<div class="4u">
					<span class="me image image-full">
                        <% if (character != null) { %>
                          <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(character) %>" alt="<%= character.NickName %>" />
                        <% } else { %>
                           <img src="<%= ViewData.Model.LinkHelper.GetUserPartnerAvatar(ViewData.Model.LoggedUser) %>" alt="<%= ViewData.Model.LoggedUser.NickName %>" />
                        <% } %>
                    </span>
				</div>
				<div class="8u" data-id="<%= ViewData.Model.LoggedUser.UserId %>">
					<header>
						<h1>Olá, <strong><%= ViewData.Model.LoggedUser.NickName %></strong>.</h1>
					</header>
					<p><strong><%= character.NickName %></strong> já desafiou alguém hoje? Convide seus amigos e mostre quem é que manda em Dragonologys.</p>
					<a href="#challenges" class="button button-big">Quero duelar agora!</a>
				</div>
			</div>
		</article>
    </div>
    
    <!-- Characters to challenge -->
    <div class="wrapper wrapper-style3">
        <article id="challenges">
			<header>
				<h2>Escolha seu oponente</h2>
				<span>Desafie os demais personagens e acompanhe os combates</span>
			</header>
			<div class="container">
                <% int i = 0; %>
                <% foreach (UserAccount account in ViewData.Model.Characters) { %>
                    <% if (account.UserAccountId != character.UserAccountId)
                       { 
                           if (i == 0) { %>
                            <div class="row">
                        <% } %>

                        <% CharacterCardViewData data = new CharacterCardViewData(ViewData.Model); %>
                        <% data.Account = account; %>

                        <% Html.RenderPartial("_CharacterCard", data); %>

                        <% if (i == 2) {
                             i = 0;
                        %>   </div>
                        <% } else {
                               i++;
                           }%>
                    <% } %>
                <% } %>
                
                <% if (i != 0)
                   { %>
                    </div>
                <% } %>
			</div>
		</article>
    </div>
    
    <!-- Contact -->
    <div class="wrapper wrapper-style4">
        <article id="alerts" class="container small">
			<header>
				<h2>Dúvida? Sugestão? Fale comigo!</h2>
				<span>O conteúdo da mensagem é privado e apenas eu irei ler.</span>
			</header>
			<div>
				<div class="row">
					<div class="12u">
						<form onsubmit="return false;">
							<div>
								<div class="row half">
									<div class="12u">
										<input type="text" name="subject" id="subject" placeholder="Assunto" />
									</div>
								</div>
								<div class="row half">
									<div class="12u">
										<textarea name="message" id="message" placeholder="Mensagem"></textarea>
									</div>
								</div>
                                <div id="sentMessageDiv">
                                    <br />
                                    <span class="sentMessage">Mensagem enviada!</span>
                                    <br />
                                </div>

								<div class="row">
									<div class="12u">
										<a id="contactSendButton" href="javascript: return false;" onclick="return false;" class="button form-button-submit">Enviar</a>
										<a id="contactClearButton" href="javascript: return false;" onclick="return false;" class="button button-alt form-button-reset">Limpar</a>

									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="12u">
						<hr />
						<h3>Encontre-me em...</h3>
						<ul class="social">
							<li class="twitter"><a href="https://twitter.com/artur_caliendo" class="fa fa-twitter"><span>Twitter</span></a></li>
							<li class="facebook"><a href="http://www.facebook.com/artur.caliendoprado" class="fa fa-facebook"><span>Facebook</span></a></li>
							<li class="googleplus"><a href="https://plus.google.com/u/0/109159807593274554095/posts" class="fa fa-google-plus"><span>Google+</span></a></li>
							<li class="github"><a href="https://github.com/arturbcc" class="fa fa-github"><span>Github</span></a></li>
                            <li class="youtube"><a href="http://www.youtube.com/arturbcc" class="fa fa-youtube"><span>YouTube</span></a></li>
                            
							<!--
                            <li class="linkedin"><a href="#" class="fa fa-linkedin"><span>LinkedIn</span></a></li>
							<li class="tumblr"><a href="#" class="fa fa-tumblr"><span>Tumblr</span></a></li>
                            <li class="dribbble"><a href="http://dribbble.com/n33" class="fa fa-dribbble"><span>Dribbble</span></a></li>
							<li class="rss"><a href="#" class="fa fa-rss"><span>RSS</span></a></li>
							<li class="instagram"><a href="#" class="fa fa-instagram"><span>Instagram</span></a></li>
							<li class="foursquare"><a href="#" class="fa fa-foursquare"><span>Foursquare</span></a></li>
							<li class="skype"><a href="#" class="fa fa-skype"><span>Skype</span></a></li>
							<li class="soundcloud"><a href="#" class="fa fa-soundcloud"><span>Soundcloud</span></a></li>
							<li class="youtube"><a href="#" class="fa fa-youtube"><span>YouTube</span></a></li>
							<li class="blogger"><a href="#" class="fa fa-blogger"><span>Blogger</span></a></li>
							<li class="flickr"><a href="#" class="fa fa-flickr"><span>Flickr</span></a></li>
							<li class="vimeo"><a href="#" class="fa fa-vimeo"><span>Vimeo</span></a></li>
							-->
						</ul>
						<hr />
					</div>
				</div>
			</div>
		</article>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Arenah de Duelos</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
    <% string path = Url.VirtualContent(); %>
    <script src="<%= path %>Content/Scripts/duels.js" type="text/javascript"></script>
</asp:Content>

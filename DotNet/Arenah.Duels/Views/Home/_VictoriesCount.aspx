﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.CharacterCardViewData>" %>

<% 
    //Character logged in
    UserAccount character = ViewData.Model.Character;
    
    //Account of the card
    UserAccount account = ViewData.Model.Account;
%>

<hr class="noMargin"/><br />
<p class="noMargin">
    Contra <%= account.Gender == Gender.Masculine ? "ele" : "ela" %> você tem: <br />
    <% BattleStatistics statistics = DuelHelper.GetBattleResult(character.UserAccountId, account.UserAccountId); %>
    <span class="venceu"><%= statistics.Victories %></span> vitória(s) | 
    <span class="perdeu"><%= statistics.Defeats %></span> derrota(s)
</p>
﻿<%@  Language="C#" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.CharacterCardViewData>" %>

<% 
    //Character logged in
    UserAccount character = ViewData.Model.Character;
    
    //Account of the card
    UserAccount account = ViewData.Model.Account;
    
    string path = Url.VirtualContent(); 
    bool hasHiddenFields = true;
%>

<div class="4u characterCard">
    <article class="box box-style2 main" data-id="<%= account.UserAccountId %>">
		<a href="javascript:;" class="image image-full character">
            <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(account) %>" alt="<%= account.NickName %>" />
        </a>
        <% Duel currentDuel = ViewData.Model.Duels.Where<Duel>(duel => (duel.ChallengerId == account.UserAccountId || duel.ChallengedId == account.UserAccountId) && duel.Status != (int) Duel.STATUS.FINISHED).FirstOrDefault<Duel>(); %>
        <% if (currentDuel != null)
            { %>
            <% if (currentDuel.Status == (int)Duel.STATUS.FINISHED)
               { %>
                <p class="noMargin">Desafie </p>
            <% } %>
            <h3><a href="javascript:;" class="character"><%= account.NickName %></a></h3>
            <% switch (currentDuel.Status)
                { %>
                <% case (int)Duel.STATUS.WAITING_FOR_TURN:  %>
                    <% if (!currentDuel.Challenged.ChoseSkills()) { %>
                        <% if (character.UserAccountId == currentDuel.ChallengedId)
                           { %>
                            <p class="waiting">Escolha suas ações</p>
                        <% }
                           else
                           { %>
                            <p class="waiting">Aguardando adversário</p>
                            <% hasHiddenFields = false; %>
                        <% } %>
                        
                    <% } else if (!currentDuel.Challenger.ChoseSkills()) {%>
                        <% if (character.UserAccountId == currentDuel.ChallengerId)
                           { %>
                            <p class="waiting">Escolha suas ações</p>
                        <% } else { %>
                            <p class="waiting">Aguardando adversário</p>
                            <% hasHiddenFields = false; %>
                        <% } %>
                    <% } %>
                <% break; %>
                <% case (int)Duel.STATUS.TURN_START:  %>
                    <p class="waiting">Escolha suas ações</p>
                <% break; %>
            <% } %>
            
            <% Html.RenderPartial("../Home/_VictoriesCount", ViewData.Model); %>

            <% if (!ViewData.Model.HideBackButton && currentDuel.Status != (int) Duel.STATUS.FINISHED) { %>
                <a href="<%= ViewData.Model.LinkHelper.GetDuelLink(currentDuel.Id) %>" class="showDuelLink">[Veja aqui como anda o duelo]</a>
            <% } %>
        <% }
            else
            { %>
                <p class="noMargin">Desafie </p>
                <h3><a href="javascript:;" class="character"><%= account.NickName %></a></h3>
                <p>para um duelo</p>
    
                <% Html.RenderPartial("../Home/_VictoriesCount", ViewData.Model); %>            
        <% } %>

        <% if (hasHiddenFields) { %>
            <input type="hidden" class="hasHiddenFields" value="true" />            
        <% } %>
	</article>
    <% if (hasHiddenFields) { %>
        <article class="box box-style2 hiddenFields">
            <% Guid challengerId = currentDuel == null ? character.UserAccountId : currentDuel.ChallengerId; %>
            <% Guid challengedId = currentDuel == null ? account.UserAccountId : currentDuel.ChallengedId; %>
            <form action="<%= ViewData.Model.LinkHelper.GetUpdateDuelLink(currentDuel == null ? 0 : currentDuel.Id) %>" method="post">
                <input type="hidden" name="loggedCharacterId" value="<%= character.UserAccountId %>"/>
                <input type="hidden" name="challengerId" value="<%= challengerId %>"/>
                <input type="hidden" name="challengedId" value="<%= challengedId %>"/>
                <div>
                    <span>Escolha três ações de ataque:</span><br />
                    <%= Html.SkillsList("attackTurn1", ViewData.Model.CharacterData.Skills, true, 0) %> <br />
                    <%= Html.SkillsList("attackTurn2", ViewData.Model.CharacterData.Skills, true, 1) %> <br />
                    <%= Html.SkillsList("attackTurn3", ViewData.Model.CharacterData.Skills, true, 2) %> 
                    <br />
                    <span>Escolha três ações de defesa:</span><br />
                    <%= Html.SkillsList("defenseTurn1", ViewData.Model.CharacterData.Skills, false, 0) %> <br />
                    <%= Html.SkillsList("defenseTurn2", ViewData.Model.CharacterData.Skills, false, 1) %> <br />
                    <%= Html.SkillsList("defenseTurn3", ViewData.Model.CharacterData.Skills, false, 2) %>

                    <br /> <br />
                    <% if (!ViewData.Model.HideBackButton) { %>
                        <button class="backToCharacterCard button" onclick="return false;">Voltar</button>
                    <% } %>
                    <button class="saveDuel button">Duelar</button>
                </div>
            </form>
        </article>
    <% } %>

    <% if (hasHiddenFields && ViewData.Model.HideBackButton) { %>
        <script>
            $(document).ready(function () {
                imageClick = true;                
            });
        </script>
    <% } %>
</div>
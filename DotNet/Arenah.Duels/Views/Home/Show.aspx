﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.BaseViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#top">Topo</a></li>
			<li><a href="<%= path %>">Voltar</a></li>
		</ul>
	</nav>

    <% Neverend.Arena.Model.UserAccount character = ViewData.Model.Character; %>
    
    <!-- Home -->
    <div class="wrapper wrapper-style1 wrapper-first">
        <article class="container" id="top">
			<div class="row">
				<div class="4u">
					<span class="me image image-full">
                        <% if (character != null) { %>
                          <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(character) %>" alt="<%= character.NickName %>" />
                        <% } else { %>
                           <img src="<%= ViewData.Model.LinkHelper.GetUserPartnerAvatar(ViewData.Model.LoggedUser) %>" alt="<%= ViewData.Model.LoggedUser.NickName %>" />
                        <% } %>
                    </span>
				</div>
				<div class="8u">
					<header>
						<h1>Edição</h1>
					</header>
                    <p>Personagem: <strong><%= character.NickName %></strong></p>

				</div>
			</div>
		</article>
    </div>
    
    <!-- Work -->
    <% string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : ""; %>
    <div class="wrapper wrapper-style2">
        <form action="<%= path %>update<%= mvc %>/<%= character.UserAccountId %>" method="post" id="form">
            <article id="duels">
			    <header>
				    <h2>Edição de personagem</h2>
			    </header>
			    <div class="container">
				    <div class="row">
					    <div class="4u">
						    <section class="box box-style1">
							    <span class="fa featured fa-comments-o"></span>
							    <h3>Estatística</h3>
							    <p>Vitórias: <span class="victories">0</span> | Derrotas: <span class="defeats">0</span></p>
						    </section>
					    </div>
					    <div class="4u">
						    <section class="box box-style1">
							    <span class="fa featured fa-file-o"></span>
							    <h3>Ficha do personagem</h3>
							    <p>
                                    <textarea name="sheetContent"><%= ViewData.Model.DuelCharacter.Sheet %></textarea>
                                </p>
						    </section>
					    </div>
				    </div>
			    </div>
			    <footer>
				    <a href="javascript:$('#form').submit();" class="button button-big">Salvar</a>
			    </footer>
		    </article>
        </form>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Edição</title>
    <meta name="robots" content="noindex,nofollow">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
</asp:Content>

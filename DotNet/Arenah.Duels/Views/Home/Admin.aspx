﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.BaseViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#top">Topo</a></li>
			<li><a href="#challenges">Personagens</a></li>
            <li><a href="<%= WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") %>dologout">Sair</a></li>
		</ul>
	</nav>

    <% Neverend.Arena.Model.UserAccount character = ViewData.Model.Character; %>
    
    <!-- Home -->
    <div class="wrapper wrapper-style1 wrapper-first">
        <article class="container" id="top">
			<div class="row">
				<div class="4u">
					<span class="me image image-full">
                        <img src="<%= ViewData.Model.LinkHelper.GetUserPartnerAvatar(ViewData.Model.LoggedUser) %>" alt="<%= ViewData.Model.LoggedUser.NickName %>" />
                    </span>
				</div>
				<div class="8u" data-id="<%= ViewData.Model.LoggedUser.UserId %>">
					<header>
						<h1>Olá, <strong><%= ViewData.Model.LoggedUser.NickName %></strong>.</h1>
					</header>
					<p>Já desafiou alguém hoje? Convide seus amigos e mostre quem é que manda em Dragonologys .</p>
					<a href="#challenges" class="button button-big">Gerenciar</a>
				</div>
			</div>
		</article>
    </div>
       
    <!-- Portfolio -->
    <% string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : ""; %>
    <div class="wrapper wrapper-style3">
        <article id="challenges">
			<header>
				<h2>Escolha o personagem</h2>
			</header>
			<div class="container">
                <% int i = 0; %>
                <% foreach (UserAccount account in ViewData.Model.Characters) { %>
                    <% if (i == 0) { %>
                        <div class="row">
                    <% } %>
					<div class="4u">
						<article class="box box-style2">
							<a href="<%= path %>show<%= mvc %>/<%= account.UserAccountId %>" class="image image-full">
                                <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(account) %>" alt="<%= account.NickName %>" />
                            </a>
							<h3><a href="javascript:;"><%= account.NickName %></a></h3>
						</article>
					</div>
                    <% if (i == 2)
                       {
                           i = 0;%>
                           </div>
                    <% }
                       else
                       {
                           i++;
                       }%>

                <% } %>				
			</div>
		</article>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Adminstração da Arenah de Duelos</title>
    <meta name="robots" content="noindex,nofollow">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
</asp:Content>

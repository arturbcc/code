﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.DuelViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#challenges">Topo</a></li>
            <li><a href="<%= path %>avisos" class="menu-messages">Mensagens</a></li>
            <li><a href="<%= path %>">Início</a></li>
		</ul>
	</nav>

    <% UserAccount character = ViewData.Model.Character; %>
    <% Duel duel = ViewData.Model.CurrentDuel; %>

    <% if (character != null && duel.Status != (int) Duel.STATUS.FINISHED) { %>
    <div class="wrapper wrapper-style3">
        <article id="alerts">
			<header>
				<h2>Escolha a próxima ação</h2>
			</header>
            <div class="container">
		        <div class="row">
                    <% CharacterCardViewData data = new CharacterCardViewData(ViewData.Model); %>
                    <% data.Account = character.UserAccountId == duel.ChallengedId ? ViewData.Model.ChallengerUserAccount : ViewData.Model.ChallengedUserAccount; %>
                    <% data.HideBackButton = true; %>
                    <% Html.RenderPartial("../Home/_CharacterCard", data); %>
                </div>
            </div>
        </article>
    </div>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Escolha suas ações</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
    <% string path = Url.VirtualContent(); %>
    <script src="<%= path %>Content/Scripts/duels.js" type="text/javascript"></script>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>

    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="<%= path %>">Início</a></li>
		</ul>
	</nav>

    <div class="wrapper wrapper-style3">
        <article id="challenges">
            
			<header>
				<h2>Melhor tomar um fôlego</h2>
			</header>
            
			<div class="container">
                Você já desafiou este adversário <%= WebConfig.GetIntKey("DuelsPerDay") %> vezes hoje. Deixe ele descansar e 
                tente denovo depois da meia noite.
            </div>
        </article>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Limite de duelos diário excedido</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
</asp:Content>

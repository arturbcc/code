﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.DuelViewData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% string path = Url.VirtualContent(); %>
    <% UserAccount character = ViewData.Model.Character; %>
    <% Duel duel = ViewData.Model.CurrentDuel; %>

    <!-- Nav -->
    <nav id="nav">
		<ul class="container">
			<li><a href="#top">Topo</a></li>
            <li><a href="#challenges">Turnos</a></li>
            <% if (character != null)
               { %>
                <% if (ViewData.Model.CurrentDuel.Status != (int)Duel.STATUS.FINISHED)
                   { %>
                        <li><a href="#alerts">Ações</a></li>
                <% } %>
                <li><a href="<%= path %>avisos" class="menu-messages">Mensagens</a></li>
            <% } %>            
			<li><a href="<%= path %>">Início</a></li>
		</ul>
	</nav>

    <!-- Home -->
    <div class="wrapper wrapper-style1 wrapper-first">
        <article class="container" id="top">
			<div class="row">
				<div class="4u">
					<span class="me image image-full">
                      <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(ViewData.Model.ChallengerUserAccount) %>" alt="<%= ViewData.Model.ChallengerUserAccount.NickName %>" />
                    </span>
				</div>
				<div class="4u">
					<span class="me image image-full">
                      <img src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(ViewData.Model.ChallengedUserAccount) %>" alt="<%= ViewData.Model.ChallengedUserAccount.NickName %>" />
                    </span>
				</div>
			</div>
		</article>
    </div>

    <div class="wrapper wrapper-style3">
        <article id="challenges">
            <% if (string.IsNullOrEmpty(ViewData.Model.CurrentDuel.Log)) { %>
			    <header>
				    <h2>Ainda não há turnos de combate</h2>
			    </header>
            <% } %>
			<div class="container">
                <%= ViewData.Model.CurrentDuel.Log %>


                <% if (ViewData.Model.CurrentDuel.Status != (int)Duel.STATUS.FINISHED && !string.IsNullOrEmpty(ViewData.Model.CurrentDuel.Log))
                   { %>
                    <div id="continueBattle">
                        O duelo ainda não terminou. Você pode: <br /><br />
                        * <a href="<%= ViewData.Model.LinkHelper.GetChooseActionLink(ViewData.Model.CurrentDuel.Id) %>">CLICAR AQUI</a> para escolher suas próximas ações<br />
                        * Checar suas mensagens privadas mais tarde.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                <% }
                   else if (ViewData.Model.CurrentDuel.Status == (int)Duel.STATUS.FINISHED && !string.IsNullOrEmpty(ViewData.Model.CurrentDuel.Log))
                   { %>
                        <% UserAccount vanquished = ViewData.Model.Vanquished; %>
                        <% UserAccount victor = ViewData.Model.Victor; %>
                        <img style="height: 130px" src="<%= ViewData.Model.LinkHelper.GetUserAccountAvatar(ViewData.Model.Victor) %>" alt="<%= victor.NickName %>" />
                        <img style="height: 130px" src="<%= path %>Content/images/<%= vanquished.UserAccountId %>_blood.png" alt="<%= vanquished.NickName %>" />
                <% } %>

                
                

            </div>
        </article>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Andamento do duelo</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
    <% string path = Url.VirtualContent(); %>
    <script src="<%= path %>Content/Scripts/duels.js" type="text/javascript"></script>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<Arenah.Duels.ViewData.BaseViewData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainHeader" runat="server">
    <title>Login::Arenah</title>
    <meta name="robots" content="noindex,nofollow">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="loginBox">
        <% if (!string.IsNullOrEmpty(ViewData.Model.Message)){  %>
           <p class="error">
            <%= ViewData.Model.Message %>
           </p>
        <% } %>
    
        <form action="<%= WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") %>dologin<%= WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "" %>" method="post">
            <span class="label">Email: </span><input type="text" name="login"/><br />
            <span class="label">Senha: </span> <input type="password" name="password"/><br />
            <input type="hidden" name="loginButton" value="1" />
            <input type="hidden" name="loginButton.x" value="1" />
            <input type="submit" name="send" id="send" />
        </form>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Javascripts" runat="server">
</asp:Content>

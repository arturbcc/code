﻿using System.Diagnostics;
using Neverend.Arena;

namespace Arenah.Duels.Models
{
    public abstract class BaseHelper
    {
        /// <summary>
        /// private instance of API class
        /// </summary>
        private API _API;

        /// <summary>
        /// Gets the API.
        /// </summary>
        /// <value>The API.</value>
        protected API API
        {
            [DebuggerStepThrough]
            get
            {
                if (_API == null)
                    _API = new API();
                return _API;
            }
        }
    }
}

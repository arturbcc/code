﻿using System;
using System.Collections.Generic;
using System.Linq;
using Neverend.Arena.Model;
using System.Web;
using System.Text;
using PassportLibrary;
using Neverend.Arena.Model.RpgSystem;
using Neverend.Arena;
using Common;


namespace Arenah.Duels.Models
{
    public class LinkHelper : BaseHelper
    {
        public string GetUserAccountAvatar(UserAccount account)
        {
            if (account != null && !string.IsNullOrEmpty(account.AvatarUrl))
            {
                if (account.AvatarUrl.StartsWith("130x130"))
                {
                    return string.Concat(WebConfig.GetKey("PassportSite"), "Content/Avatar/", account.AvatarUrl);
                }
                else
                {
                    string prefix = WebConfig.GetKey("ArenahSite");
                    string virtualDirectory = WebConfig.GetKey("VirtualDirectory");
                    if (!string.IsNullOrEmpty(virtualDirectory))
                        prefix += "arenah/";
                    return string.Concat(prefix, "resources/avatar/", account.AvatarUrl);
                }
            }
            else return WebConfig.GetKey("defaultAvatar");
        }

        public string GetUserPartnerAvatar(UserPartner user)
        {
            if (user != null && !string.IsNullOrEmpty(user.AvatarUrl))
            {
                if (!user.AvatarUrl.StartsWith("130x130"))
                {
                    string prefix = WebConfig.GetKey("ArenahSite");
                    string virtualDirectory = WebConfig.GetKey("VirtualDirectory");
                    if (!string.IsNullOrEmpty(virtualDirectory))
                        prefix += "arenah/";

                    return string.Concat(prefix, "resources/avatar/", user.AvatarUrl);
                }
                else
                    return string.Concat(WebConfig.GetKey("PassportSite"), "Content/Avatar/", user.AvatarUrl);
            }
            else return WebConfig.GetKey("defaultAvatar");
        }

        public string GetDuelLink(int duelId)
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "duelo" + mvc + "/" + duelId;
        }

        public string GetChooseActionLink(int duelId)
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "acoes" + mvc + "/" + duelId;
        }

        public string GetUpdateDuelLink(int duelId)
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "duels" + mvc + "/update/" + duelId;
        }

        public string GetMessagesLink()
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "avisos" + mvc;
        }

        public string GetDeleteMessageLink(int messageId)
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "avisos" + mvc + "/delete/" + messageId;
        }

        public string SendMessageLink()
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "contato" + mvc;
        }

        public string DailyLimitExceededLink()
        {
            string virtualDirectory = WebConfig.GetKey("VirtualDirectory");

            string slash = "/";
            if (!string.IsNullOrEmpty(virtualDirectory))
                slash += virtualDirectory;

            string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
            return slash + "descanse" + mvc;
        }

    }
}

﻿using System.Collections.Generic;
using Arenah.Duels.Models;
using System.Text;
using System.Web.Mvc;
using System;
using System.Linq;
using Neverend.Arena.Model;
using Neverend.Arena;

namespace Arenah.Duels.Helpers
{
    public static class DuelHelper
    {
        public static string SkillsList(this HtmlHelper helper, string id, List<Skill> skills, bool attack, int position)
        {
            StringBuilder select = new StringBuilder();
            select.AppendFormat("<select name='{0}' id='{0}'>", id);

            int i = 0;
            foreach (Skill skill in skills)
            {
                if (attack && skill.Effect > 0)
                {
                    select.AppendFormat("<option value='{0}'{1}>[Efeito] {0}</option>", skill.Name, i == position ? "selected='selected' " : "");
                }
                else if ((attack && skill.Attack > 0) || (!attack && skill.Defense > 0 && skill.Effect == 0))
                {
                    select.AppendFormat("<option value='{0}'{2}>{0} ({1})</option>", skill.Name, (attack ? skill.Attack : skill.Defense) + skill.BaseValue, i == position ? "selected='selected' " : "");
                }
                i++;
	        }  
            select.Append("</select>");

            return select.ToString();
        }

        public static BattleStatistics GetBattleResult(Guid currentUserAccountId, Guid characterId)
        {
            BattleStatistics statistics = new BattleStatistics();
            List<Duel> duels = (from duel in Duel.db
                                where (
                                        (duel.ChallengerId == currentUserAccountId && duel.ChallengedId == characterId) ||
                                        (duel.ChallengedId == currentUserAccountId && duel.ChallengerId == characterId)
                                        ) &&
                                        duel.Status == (int)Duel.STATUS.FINISHED
                                select duel).ToList<Duel>();
            statistics.Victories = 0;
            statistics.Defeats = 0;
            foreach (Duel duel in duels)
            {
                if (duel.Winner == (int)Duel.WINNER_STATUS.CHALLENGER && duel.ChallengerId == currentUserAccountId ||
                    duel.Winner == (int)Duel.WINNER_STATUS.CHALLENGED && duel.ChallengedId == currentUserAccountId)
                    statistics.Victories++;
                else if (duel.Winner != (int)Duel.WINNER_STATUS.NONE)
                    statistics.Defeats++;
            }

            return statistics;
        }
    }
}
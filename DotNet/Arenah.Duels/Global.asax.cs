﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Arenah.Duels.Models;
using Boycott;
using Boycott.Provider;
using log4net;

namespace Arenah.Duels
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private static string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Home", "home" + mvc, new { controller = "Home", action = "Index" });
            routes.MapRoute("Show", "show" + mvc + "/{characterId}", new { controller = "Home", action = "Show" });
            routes.MapRoute("Update", "update" + mvc + "/{characterId}", new { controller = "Home", action = "Update" });
            routes.MapRoute("Duels Update", "duels" + mvc + "/update/{duelId}", new { controller = "Duels", action = "Update" });
            routes.MapRoute("Duels Show", "duelo" + mvc + "/{duelId}", new { controller = "Duels", action = "Show" });
            routes.MapRoute("Duels Choose Action", "acoes" + mvc + "/{duelId}", new { controller = "Duels", action = "ChooseAction" });
            routes.MapRoute("Login Index", "login" + mvc, new { controller = "Login", action = "Index" });
            routes.MapRoute("DoLogin", "dologin" + mvc, new { controller = "Login", action = "DoLogin" });
            routes.MapRoute("DoLogout", "dologout" + mvc, new { controller = "Login", action = "DoLogout" });
            routes.MapRoute("Messages Index", "avisos" + mvc, new { controller = "Messages", action = "Index" });
            routes.MapRoute("Messages Delete", "avisos" + mvc + "/delete/{messageId}", new { controller = "Messages", action = "Delete" });
            routes.MapRoute("Messages Count", "avisos" + mvc + "/contagem", new { controller = "Messages", action = "Count" });
            routes.MapRoute("Messages Show", "aviso" + mvc + "/{messageId}", new { controller = "Messages", action = "Show" });
            routes.MapRoute("Contact Create", "contato" + mvc, new { controller = "Contact", action = "Create" });
            routes.MapRoute("Duels Limit", "descanse" + mvc, new { controller = "Duels", action = "Limit" });
            routes.MapRoute("Prepare", "prepare" + mvc, new { controller = "Home", action = "Prepare" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);
            log4net.Config.XmlConfigurator.Configure();

            System.Configuration.ConnectionStringSettings connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["duels"];
            string[] parts = connectionString.ConnectionString.Split(';');
            string server = parts[0].Split('=')[1];
            string database = parts[3].Split('=')[1];
            string user = parts[1].Split('=')[1];
            string password = parts[2].Split('=')[1];
            Configuration.DatabaseProvider = new MySQLProvider(server, database, user, password);

        }

        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));

        void Application_Error(Object sender, EventArgs e)
        {

            Exception exceptionInstance = Server.GetLastError();
            HttpException httpException = exceptionInstance as HttpException;
            int? httpStatusCode = (httpException != null ? httpException.GetHttpCode() : (int?)null);

            if (!httpStatusCode.HasValue || httpStatusCode != 404)
            {
                Exception ex = Server.GetLastError().GetBaseException();
                log.Error("App_Error", ex);
            }
        }
    }
}
﻿using System.Web.Mvc;
using PassportManager.Manager;
using PassportLibrary;
using Arenah.Duels.ViewData;
using Arenah.Duels.Models;

namespace Arenah.Duels.Controllers
{
    public class ContactController : Controller
    {
        [RequiresAuthentication]
        public ActionResult Create(FormCollection form)
        {
            BaseViewData viewData = new BaseViewData();
            PrivateMessage pm = new PrivateMessage();
            pm.Content = form["message"];
            pm.From = viewData.LoggedUser.PassportId;
            pm.ProductId = 10;
            pm.Status = PassportLibrary.Enum.PrivateMessageStatus.NotRead;
            pm.Subject = "[Duelos] " + form["subject"];
            pm.To = 20;

            PrivateMessageManager pmManager = new PrivateMessageManager();
            pmManager.CreatePrivateMessage(pm);
            return new RedirectResult(WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory"));
        }

    }
}

﻿using System.Web.Mvc;
using Arenah.Duels.ViewData;
using Arenah.Duels.Models;
using System.Collections.Generic;
using Neverend.Arena.Model;
using System.Linq;
using System;
using System.Web.Script.Serialization;

namespace Arenah.Duels.Controllers
{
    [HandleError, RequiresAuthentication]
    public class HomeController : Controller
    {
        BaseViewData viewData = new BaseViewData();

        public ActionResult Index()
        {
            //Load characters from Arenah System
            viewData.Characters = (from character in Neverend.Arena.API.UserAccountManager.GetUserAccountsByForumId(Factory.forumId, UserAccountType.PC, false, false)
                                            where character.Status == UserAccountStatus.Active
                                            select character).ToList<UserAccount>();

            if (viewData.Character != null)
            {

                //Character Logged in
                //Character currentCharacter = Character.FindById(viewData.Character.UserAccountId);
                //viewData.CharacterData = new CharacterData(currentCharacter.Sheet);
                viewData.CharacterData = CharacterData.FromUserAccountId(viewData.Character.UserAccountId);

                viewData.Duels = Duel.DuelsForUserAccountId(viewData.Character.UserAccountId);

                return View(viewData);
            }
            else
                return View("Admin", viewData);
        }

        [HandleError, RequiresAuthentication]
        public ActionResult Show(Guid characterId)
        {
            UserAccount character = Neverend.Arena.API.UserAccountManager.GetUserAccountById(characterId);
            UserPartner loggedUser = viewData.LoggedUser;

            if (!loggedUser.IsPartnerAdmin && !loggedUser.IsSystemAdmin)
                return new EmptyResult();
            else
            {
                viewData.Character = character;
                viewData.DuelCharacter = Character.FindOrCreate(characterId);
                return View(viewData);
            }
        }

        [HandleError, RequiresAuthentication]
        public ActionResult Update(Guid characterId, FormCollection form)
        {
            UserAccount character = Neverend.Arena.API.UserAccountManager.GetUserAccountById(characterId);
            UserPartner loggedUser = viewData.LoggedUser;

            if (!loggedUser.IsPartnerAdmin && !loggedUser.IsSystemAdmin)
                return new RedirectResult(WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory"));
            else
            {
                Character obj = Character.FindOrCreate(characterId);

                obj.Sheet = form["sheetContent"];
                obj.Save();
                
                string mvc = WebConfig.GetBoolKey("UseMvcOnUrl") ? ".mvc" : "";
                return new RedirectResult(WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") + "show" + mvc + "/" + characterId);
            }
        }

        [HandleError, RequiresAuthentication]
        public ActionResult Prepare()
        {
            List<UserAccount> characters = (from character in Neverend.Arena.API.UserAccountManager.GetUserAccountsByForumId(Factory.forumId, UserAccountType.PC, false, false)
                                   where character.Status == UserAccountStatus.Active
                                   select character).ToList<UserAccount>();

            foreach (UserAccount account in characters)
            {
                CharacterData.FromUserAccountId(account.UserAccountId);
            }

            return new RedirectResult(WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory"));
        }
    }
}

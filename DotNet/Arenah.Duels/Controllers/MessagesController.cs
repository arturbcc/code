﻿using System.Web.Mvc;
using Arenah.Duels.ViewData;
using Arenah.Duels.Models;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Arenah.Duels.Controllers
{
    public class MessagesController : Controller
    {
        MessageViewData viewData = new MessageViewData();

        [RequiresAuthentication]
        public ActionResult Index()
        {
            viewData.Messages = Message.FindAll(new { ChallengedId = viewData.Character.UserAccountId });
            SetReadStatus(viewData.Messages);
            FindDuels(viewData.Messages);
            return View(viewData);
        }

        [RequiresAuthentication]
        public ActionResult Delete(int messageId)
        {
            Message message = Message.Find(messageId);

            if (message.ChallengedId == viewData.Character.UserAccountId)
            {
                Message.Delete(messageId);
            }

            return new RedirectResult(viewData.LinkHelper.GetMessagesLink());
        }

        public ActionResult Show(int messageId)
        {
            viewData.CurrentMessage = Message.Find(messageId);
            Duel duel = Duel.Find(viewData.CurrentMessage.DuelId);

            if (duel != null) 
            {
                duel.Challenged = new CharacterData(duel.ChallengedSheet);
                duel.Challenger = new CharacterData(duel.ChallengerSheet);

                if (duel.Challenged.IsAlive())
                {
                    viewData.Vanquished = Character.FindById(duel.ChallengerId);
                    viewData.Victor = Character.FindById(duel.ChallengedId);
                }
                else
                {
                    viewData.Vanquished = Character.FindById(duel.ChallengedId);
                    viewData.Victor = Character.FindById(duel.ChallengerId);
                }

                Share share = new Share()
                {
                    CreationDate = DateTime.Now,
                    DuelId = duel.Id,
                    MessageId = messageId
                };

                if (viewData.Character != null)
                    share.UserAccountId = viewData.Character.UserAccountId;

                share.Save();
                //ImageCombiner.CombineTwoImages(viewData.Vanquished.UserAccountId);
            }
            
            return View(viewData);
        }

        public int Count()
        {
            int count = (from message in Message.db 
                         where message.ChallengedId == viewData.Character.UserAccountId
                         select message).Count<Message>();

            return count;
        }

        private void SetReadStatus(List<Message> messages)
        {
            for (int i = messages.Count - 1; i >= 0; i--)
            {
                if (messages[i].Read == null)
                {
                    messages[i].Read = false;
                    ChangeListStatus(messages, messages[i].DuelId, messages[i].Id);
                }
            }
        }

        private void ChangeListStatus(List<Message> messages, int duelId, int messageId)
        {
            foreach (Message message in messages)
            {
                if (message.DuelId == duelId && message.Id != messageId)
                    message.Read = true;
            }
        }

        private void FindDuels(List<Message> messages)
        {
            foreach (Message message in messages)
            {
                if (message.Status == (int)Message.STATUS.DUEL_FINISHED)
                {
                    message.Duel = Duel.Find(message.DuelId);
                    message.Duel.Challenger = new CharacterData(message.Duel.ChallengerSheet);
                    message.Duel.Challenged = new CharacterData(message.Duel.ChallengedSheet);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Arenah.Duels.ViewData;
using Common;
using System.Configuration;
using PassportManager.Manager;
using PassportLibrary;
using Arenah.Duels.Models;

namespace Arenah.Duels.Controllers
{
    public class LoginController : Controller
    {
        BaseViewData viewData = new BaseViewData();
        private UserCookieManager userCookieManager = new UserCookieManager();

        private string ReturnUrl
        {
            get
            {
                if (string.IsNullOrEmpty(Request["surl"]))
                    return WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory");
                else
                    return HttpUtility.UrlDecode(Request["surl"].ToString().Replace(".mvc", ""));
            }
        }

        public ActionResult Index()
        {
            if (Request["error"] != null)
                viewData.Message = "Email ou senha incorretos";

            return View(viewData);
        }

        public ActionResult DoLogin(FormCollection form)
        {
            string mvc = Util.ConvertToBoolean(ConfigurationManager.AppSettings["UseMvcOnUrl"]) ? ".mvc" : "";

            string login = form["login"];
            string password = form["password"];

            if (string.IsNullOrEmpty(login) && string.IsNullOrEmpty(password))
            {
                viewData.Message = "Login ou senha incorretos. Todos os campos são obrigatórios.";
                return View("index", viewData);
            }

            UserManager userManager = new UserManager();
            List<User> users;
            if (userManager.ValidateUser(login, password))
            {
                users = userManager.GetUsers(login, UserManager.SearchType.ByEmail);

                if (users.Count > 0)
                {
                    SignInUser(users[0], true);
                    users[0].BeforeLastLoginDate = users[0].LastLoginDate;
                    users[0].LastLoginDate = DateTime.Now;
                    userManager.Update(users[0]);
                    return new RedirectResult(ReturnUrl);
                }
                else return new EmptyResult();
            }
            else
            {
                viewData.Message = "Login ou senha incorretos.";
                return new RedirectResult(WebConfig.GetKey("ArenahDuelsSite") + WebConfig.GetKey("VirtualDirectory") + "login" + mvc + "?error=1&surl=" + ReturnUrl);
            }
        }

        public ActionResult DoLogout()
        {
            userCookieManager.SignOut();
            return new RedirectResult(ReturnUrl);
        }

        private void SignInUser(User user, bool isPersistent)
        {
            userCookieManager.SignIn(user.UserId, user.FullName, isPersistent);
        }
    }
}

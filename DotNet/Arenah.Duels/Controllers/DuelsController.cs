﻿using System.Web.Mvc;
using System;
using System.Linq;
using Arenah.Duels.Models;
using Arenah.Duels.ViewData;
using Neverend.Arena.Model;
using System.Collections.Generic;

namespace Arenah.Duels.Controllers
{
    public class DuelsController : Controller
    {
        public ActionResult Show(int duelId)
        {
            DuelViewData viewData = new DuelViewData();
            viewData.CurrentDuel = Duel.Find(duelId);

            if (viewData.CurrentDuel != null)
            {
                if (viewData.Character != null)
                {
                    viewData.Duels = Duel.DuelsForUserAccountId(viewData.Character.UserAccountId);

                    //Logged in Character
                    Character currentCharacter = Character.FindById(viewData.Character.UserAccountId);
                    viewData.CharacterData = new CharacterData(currentCharacter.Sheet);
                }
                
                viewData.ChallengerUserAccount = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengerId);
                viewData.ChallengedUserAccount = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengedId);

                if (viewData.CurrentDuel.Status == (int)Duel.STATUS.FINISHED)
                {
                    if (viewData.CurrentDuel.Winner == 1)
                    {
                        viewData.Victor = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengerId);
                        viewData.Vanquished = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengedId);
                    }
                    else if (viewData.CurrentDuel.Winner == 2)
                    {
                        viewData.Victor = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengedId);
                        viewData.Vanquished = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengerId);
                    }
                }

                return View(viewData);
            }
            else
            {
                string virtualDirectory = "/" + WebConfig.GetKey("VirtualDirectory");
                return new RedirectResult(virtualDirectory);
            }
        }

        [HttpPost, RequiresAuthentication]
        public ActionResult Update(int duelId, FormCollection form)
        {
            Duel currentDuel = null;

            Guid loggedCharacterId = new Guid(form["loggedCharacterId"]);
            Guid challengerId = new Guid(form["challengerId"]);
            Guid challengedId = new Guid(form["challengedId"]);

            Duel possibleDuel = Duel.FindLast(challengerId, challengedId);

            //if the user tries to start a duel, but there is one on going
            //with the same opponent, a new duel should not be created.
            //Invert the challenger/challenged and update the current duel.
            if (possibleDuel == null)
            {
                possibleDuel = Duel.FindLast(challengedId, challengerId);
                if (possibleDuel != null)
                {
                    Guid aux = challengerId;
                    challengerId = challengedId;
                    challengedId = aux;
                }
            }

            if (possibleDuel != null && possibleDuel.Status != (int)Duel.STATUS.FINISHED)
                duelId = possibleDuel.Id;

            if (duelId > 0)
                currentDuel = Duel.Find(duelId);

            bool hasChallengerSheet = currentDuel != null && !string.IsNullOrEmpty(currentDuel.ChallengerSheet);
            bool hasChallengedSheet = currentDuel != null && !string.IsNullOrEmpty(currentDuel.ChallengedSheet);

            Character challenger = Character.FindById(challengerId);
            CharacterData challengerData = new CharacterData(hasChallengerSheet ? currentDuel.ChallengerSheet : challenger.Sheet);

            Character challenged = Character.FindById(challengedId);
            CharacterData challengedData = new CharacterData(hasChallengedSheet ? currentDuel.ChallengedSheet : challenged.Sheet);

            ChooseSkillsToFight(loggedCharacterId == challengerId ? challengerData : challengedData, form);
            
            Duel duel = null;
            Message messageToChallenger = Message.CreateMessage(challengedId, challengerId);
            Message messageToChallenged = Message.CreateMessage(challengerId, challengedId);
           
            if (currentDuel == null || currentDuel.Status == (int) Duel.STATUS.FINISHED)
            {
                if (WebConfig.GetIntKey("DuelsPerDay") > 0)
                {
                    DateTime today = DateTime.Today.Date;
                    List<Duel> todayDuels = (from todayDuel in Duel.db
                                            where (
                                                (todayDuel.ChallengerId == challengerId && todayDuel.ChallengedId == challengedId) ||
                                                (todayDuel.ChallengedId == challengerId && todayDuel.ChallengerId == challengedId)
                                            )
                                            && todayDuel.CreationDate >= today
                                            select todayDuel).ToList<Duel>();

                    if (todayDuels.Count >= 3)
                        return new RedirectResult(new BaseViewData().LinkHelper.DailyLimitExceededLink());

                }

                duel = new Duel();
                duel.ChallengerId = challengerId;
                duel.ChallengedId = challengedId;
                duel.CreationDate = DateTime.Now;
                duel.Status = (int)Duel.STATUS.WAITING_FOR_TURN;
                duel.Turn = 0;
                duel.ChallengerSheet = challengerData.ToJson();
                duel.ChallengedSheet = challengedData.ToJson();
                messageToChallenged.Title = "desafia você para um duelo (clique para aceitar)";
                messageToChallenged.Status = (int)Message.STATUS.NEW_DUEL;
            }
            else
            {
                duel = currentDuel;

                if (loggedCharacterId == duel.ChallengerId)
                    duel.ChallengerSheet = challengerData.ToJson();
                else if (loggedCharacterId == duel.ChallengedId)
                    duel.ChallengedSheet = challengedData.ToJson();


                if (challengerData.ChoseSkills() && challengedData.ChoseSkills())
                {
                    //Calculate combat
                    Combat combat = new Combat(duel);
                    combat.Start(challengerData, challengedData);

                    //Clean ChosenSkills
                    challengerData.ChosenSkills = new System.Collections.Generic.List<Skill>();
                    challengedData.ChosenSkills = new System.Collections.Generic.List<Skill>();
                    duel.ChallengerSheet = challengerData.ToJson();
                    duel.ChallengedSheet = challengedData.ToJson();

                    if (!challengerData.IsAlive() || !challengedData.IsAlive())
                    {
                        duel.Status = (int)Duel.STATUS.FINISHED;
                        duel.Winner = challengerData.IsAlive() ? 1 : 2;

                        ImageCombiner.CombineTwoImages(challengerData.IsAlive() ? duel.ChallengedId : duel.ChallengerId);
                    }
                    else
                        duel.Status = (int)Duel.STATUS.TURN_START;

                    messageToChallenged.Title = RandomMessage(challengedData.PV, challengerData.PV);
                    messageToChallenged.Status = challengedData.IsAlive() && challengerData.IsAlive() ? (int)Message.STATUS.DUEL_ON_GOING : (int)Message.STATUS.DUEL_FINISHED;
                    messageToChallenger.Title = RandomMessage(challengerData.PV, challengedData.PV);
                    messageToChallenger.Status = challengedData.IsAlive() && challengerData.IsAlive() ? (int)Message.STATUS.DUEL_ON_GOING : (int)Message.STATUS.DUEL_FINISHED;
                }
                else
                {
                    duel.Status = (int)Duel.STATUS.WAITING_FOR_TURN;
                }
            }

            bool flag = duel.Save();
            messageToChallenged.DuelId = duel.Id;
            messageToChallenger.DuelId = duel.Id;

            if (!string.IsNullOrEmpty(messageToChallenged.Title))
                messageToChallenged.Save();

            if (!string.IsNullOrEmpty(messageToChallenger.Title))
                messageToChallenger.Save();

            return new RedirectResult(new LinkHelper().GetDuelLink(duel.Id));
        }


        public ActionResult Limit()
        {
            return View("LimitExceeded");
        }

        [RequiresAuthentication]
        public ActionResult ChooseAction(int duelId)
        {
            DuelViewData viewData = new DuelViewData();
            viewData.CurrentDuel = Duel.Find(duelId);

            if (viewData.CurrentDuel != null)
            {
                if (viewData.Character != null)
                {
                    viewData.Duels = Duel.DuelsForUserAccountId(viewData.Character.UserAccountId);

                    Character currentCharacter = Character.FindById(viewData.Character.UserAccountId);
                    viewData.CharacterData = new CharacterData(currentCharacter.Sheet);
                }

                viewData.ChallengerUserAccount = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengerId);
                viewData.ChallengedUserAccount = Neverend.Arena.API.UserAccountManager.GetUserAccountById(viewData.CurrentDuel.ChallengedId);


                return View(viewData);
            }
            else
            {
                string virtualDirectory = "/" + WebConfig.GetKey("VirtualDirectory");
                return new RedirectResult(virtualDirectory);
            }
        }

        private void ChooseSkillsToFight(CharacterData character, FormCollection form)
        {
            character.ChosenSkills = new System.Collections.Generic.List<Skill>();
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["attackTurn1"]).First<Skill>());
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["attackTurn2"]).First<Skill>());
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["attackTurn3"]).First<Skill>());
                                                                                             
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["defenseTurn1"]).First<Skill>());
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["defenseTurn2"]).First<Skill>());
            character.ChosenSkills.Add(character.Skills.Where<Skill>(skill => skill.Name == form["defenseTurn3"]).First<Skill>());
        }

        private string RandomMessage(int yourPV, int opponentsPV)
        {
            string[] winnerMessages = new string[10] {
                "está tomando um coro",
                "está pedindo água",
                "quer jogar a toalha",
                "não é de nada",
                "está perdendo a luta",
                "está sangrando",
                "não quer mais brincar",
                "está com medo",
                "está arregando",
                "é um pato"
            };

            string[] loserMessages = new string[10] {
                "está acabando com você",
                "ri da sua performance",
                "disse que sua mãe é gorda",
                "não está nem começando",
                "está com pena de você",
                "está rindo de você",
                "está com a espada no seu pescoço",
                "começou a rezar pela sua alma",
                "prefere enfrentar um goblin do que você",
                "está vencendo"
            };

            string[] victorMessages = new string[10] {
                "não foi páreo para você",
                "foi chorar no colo da mãe",
                "perdeu a luta",
                "pediu arrego",
                "não deu nem para o cheiro",
                "não deu nem pra brincar",
                "foi tão difícil de vencer como uma ostra em coma",
                "morreu como um marreco",
                "is no more",
                "jaz em seu túmulo"
            };

            string[] vanquishedMessages = new string[10] {
                "derrotou você",
                "pisou na sua carcaça",
                "botou você para correr",
                "foi lutar contra um goblin atrás de um desafio de verdade",
                "não chegou nem a suar",
                "venceu com honra",
                "cuspiu no seu cadáver",
                "roubou seus itens e foi embora",
                "arrasou",
                "chutou seu traseiro gordo"
            };

            int index = DiceRoller.Throw(DiceRoller.DiceType.D10) - 1;

            if (yourPV <= 0)
                return vanquishedMessages[index];
            else if (opponentsPV <= 0)
                return victorMessages[index];
            else if (yourPV >= opponentsPV)
                return winnerMessages[index];
            else
                return loserMessages[index];
        }
    }
}
